

>>> from index.rst
..
      background/4kinds
      background/sphinx+rest
      background/gitlab

..
   where...

   #. :ref:`background/4kinds:4 kinds of docs` is a technical documentation model
   #. :ref:`background/sphinx+rest:Sphinx + reST` calls out a presentation layer:

      - *Sphinx*: SSG (static site generator) framework
      - *reST*: 'reStructuredText' markup language

   #. :ref:`GitLab: vcs & CI/CD` is for archival and publishing

      - version control
      - CI/CD publishing to the web

<<< from index.rst
