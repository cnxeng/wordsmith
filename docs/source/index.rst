.. WordSmith documentation master file, created by
   sphinx-quickstart on Sat Jan 15 09:24:42 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. |whoami| raw:: html

   <div margin="auto" style="font-variant:small-caps; font-size:28px; color:purple; text-align:center; font-weight:bold">
      Sean Lanter
   </div>
   <div margin="auto" style="font-variant:small-caps; font-size:17px; color:maroon; text-align:center">
      Tests. Tools. Docs. Data.
   </div>

|whoami|

.. _WordSmith:

#############################################
Welcome to the *WordSmith* portfolio project!
#############################################

.. |Divio| raw:: html

   <a href="https://documentation.divio.com/tutorials" target=_blank>Divio</a>

.. |GitLab| raw:: html

   <a href="https://gitlab.com/" target=_blank>GitLab</a>

.. |installed| raw:: html
            
   <a href="https://www.python.org/downloads/" target=_blank>must be installed</a>

`WordSmith`_ is a technical documentation portfolio that:

   - is built around three specific documentation `Choices`_
   - comprises an appropriate pair of `Document collections`_
   - includes a `Crosscuts`_ view of those collections

But first, a brief (and clickable!) roadmap:

.. contents:: Portfolio topics
   :local:
   :backlinks: none
   :depth: 3

.. note::

   Please be sure to visit the :ref:`background/4kinds:4 kinds of docs` link, because the information presented there is foundational to understanding what `WordSmith`_ is all about!

Enjoy!!

Project Background
==================

.. toctree::
   :maxdepth: 2
   :caption:  project background
   :hidden:

   background/about

- :ref:`background/about:About WordSmith`

Choices
=======

.. toctree::
   :maxdepth: 2
   :caption:  CHOICES
   :hidden:
 
   background/core-elements

`WordSmith`_ embodies three core elements:

  #. technical documentation model

     - *just what does "technical document" mean?*

  #. presentation layer

     - *how are technical documents displayed to users?*

  #. archival & publishing

     - *repository & version control, plus web presence*

Each of these three elements can be addressed by a vast array of different approaches (a few minutes with a search engine will verify this!). But to make itself precise and consistent, `WordSmith`_ has made, respectively, the following choices:

- technical documentation model:

  - :ref:`background/4kinds:4 kinds of docs`

- presentation layer:

  - :ref:`background/sphinx+rest:rST/Sphinx`

- archival & publishing:

  - :ref:`background/gitlab:GitLab`


Document collections
====================

Given the three `Choices`_ just discussed, `WordSmith`_ is organized as two collections of documentation, where each collection specifically reflects the :ref:`background/4kinds:4 kinds of docs` model.

  .. note::
     All code blocks in both document collections are equipped with a *copy* button in the top right corner. Clicking that button causes the content of the code block to be copied to the clipboard.

  :ref:`outer-wander:the Wander tool`:

    - documentation for a small geo-locating tool, purpose-built for `WordSmith`_
    - in each 'doc kind', there is an overarching focus that is the :ref:`Wander <outer-wander:the Wander tool>` project itself

  :ref:`outer-legacy:the Legacy docs`:

    - an eclectic collection of miscellaneous legacy tools documents (redacted as needed!)
    - each 'doc kind' is unto itself, there is no intentional coherence

.. toctree::
   :maxdepth: 2
   :caption:  document collections
   :hidden:

   outer-wander
   outer-legacy

Crosscuts
=========

So the `document collections`_ stand as they are, and each collection contains all four kinds of technical documents (following the :ref:`background/4kinds:4 kinds of docs` model). Given this, it is also helpful to separately view each of the four kinds of technical document as it appears across the two collections (think 'pivot table'):

.. toctree::
   :maxdepth: 2
   :caption:  crosscuts
   :hidden:

   outer-tutorial
   outer-how-to
   outer-reference
   outer-explanation

- :ref:`outer-tutorial:tutorials`
- :ref:`outer-how-to:how-to guides`
- :ref:`outer-reference:reference guides`
- :ref:`outer-explanation:explanation`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
