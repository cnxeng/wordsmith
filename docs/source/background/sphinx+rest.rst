
##########
rST/Sphinx
##########

.. |markdown| raw:: html

   <a href="https://commonmark.org/" target=_blank>markdown</a>

.. |coverage| raw:: html

   <a href="https://idratherbewriting.com/2016/10/28/markdown-or-restructuredtext-or-dita-choosing-format-tech-docs/" target=_blank>coverage from Tom Johnson</a>

.. |dita| raw:: html

   <a href="https://www.dita-ot.org/" target=_blank>DITA</a>

.. |Jeckyll| raw:: html

   <a href="https://jekyllrb.com/" target=_blank>Jekyll</a>
  
Both **Sphinx** and **rST** are mature, well-developed tools... definitely too much to cover in this topic! Therefore, please engage with these two resources as starting points for exploring the territory. And there are many, many other websites available to help, as your browser's search engine will reveal.

**Sphinx**: `<https://www.sphinx-doc.org/en/master/index.html>`_

**rST**: `<https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_

These techologies are what *WordSmith* has chosen to use, but neither is unique:

- For **Sphinx** alternatives, many projects use |Jeckyll|.

- For **rST** alternatives:

  - there is always |markdown|, which can also be processed by **Sphinx**
  - there is also |dita|, an XML-based markup language that requires its own toolchain
  - for a comparative look at these, refer to the excellant (as usual!) |coverage|
