
###############
4 kinds of docs
###############

.. |Divio| raw:: html

   <a href="https://documentation.divio.com/" target=_blank>Divio</a>

.. |WriteTheDocs| raw:: html

   <a href="https://www.writethedocs.org/videos/eu/2017/the-four-kinds-of-documentation-and-why-you-need-to-understand-what-they-are-daniele-procida" target=_blank>conference talk</a>

The underlying model employed by *WordSmith* observes that technical documentation is very cleanly partitioned into four separate, but still interconnected, kinds of writing. For clear and comprehensive explanations, here are two outstanding resources:
   
   - an article hosted by |Divio|

     - includes a very helpful graph in the section *About the system*, about one page down from the top

   - a talk given during a conference sponsored by |WriteTheDocs|

And from the |Divio| article:

  - **tutorials**:

    - "Tutorials are *lessons* that take the reader by the hand through a series of steps to complete a project of some kind."

  - **how-to guides**:

    - "How-to guides take the reader through the steps required to solve a real-world problem."

  - **reference guides**:

    - "Reference guides are *technical descriptions of the machinery* and how to operate it."

  - **explanation**:

    - "Explanation, or discussions, *clarify and illuminate a particular topic*."
