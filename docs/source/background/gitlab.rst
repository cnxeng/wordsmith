
######
GitLab
######

.. |GitLab| raw:: html

   <a href="https://gitlab.com/" target=_blank>GitLab</a>

|GitLab| is *WordSmith*'s chosen platform for version control and also for CI/CD publishing to the Web.

*Why*, comes the question?

*Because*, comes the answer: it best fits this author's approach to developing technical documentation.

And here is what that means:

- First, |GitLab| comprises a remote 'git' repository, which means that there is a persisted, version-controlled repository of all the content on this website.
- Second, |GItLab| offers a complete CI/CD platform that can build the documentation and deploy it to the web.
- Third, |GItLab| fully integrates the repository and the CI/CD workflow, so that the CI/CD processing is triggered automatically whenever a 'git push' is called out for the repository.

Yes, of course there are multiple alternatives. All are equally viable, and they may very well suit other projects' needs more appropriately. But the *WordSmith* needs -- and this author's approach -- are served wonderfully well by the |GitLab|, and it is what will be used.
