

#############
Core Elements
#############

   .. toctree::
      :maxdepth: 2
      :caption:  CHOICES
      :hidden:

      4kinds
      sphinx+rest
      gitlab
