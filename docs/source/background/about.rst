
###############
About WordSmith
###############

.. |resume| raw:: html

   <a href="../_static/SeanLanter_ats.docx" target=_blank>resume</a>

.. |linkedin| raw:: html

   <a href="https://www.linkedin.com/in/seanlanter" target=_blank>LinkedIn profile</a>

During the past 30+ years as a dedicated professional in the software world -- 50% developer, 50% QA specialist, 50% SDET (yes, there *has* been that much OT!) -- this author has always held two principles to be of paramount and equal importance:

   - **construxion**:

     - software is useless if it doesn't work right and solve a real problem
     - therefore the creator has to be meticulous about *correctness*

   - **connexion**:

     - software is useless if it cannot be understood and consumed by its intended audience
     - therefore the creator has to be meticulous about *documentation*

For the many dozens of projects that engaged this author as a senior resource, each project received efforts equally dedicated towards both **construxion** and **connexion**.

   - But did the job titles reflect that dual nature? Of course not.
   - Yet were the project teams -- and management and customers! -- delighted with the entirety of what was delivered? Absolutely!!

So how to demonstrate this background?

   - for **construxion**, my |resume| and my |linkedin| offer lots of information
   - for **connexion**, a portfolio is necessary

And to that end, please witness this project -- i.e., *WordSmith* -- as a portfolio of how this author approaches technical documentation for software.

To be sure, *WordSmith* is a work-in-progress effort! More documentation examples will be added over time, of course, but the starting point is here and now. Specifically, this portfolio includes an overview of the four kinds of technical documentation, along with example documents of each of those four kinds.

Finally, please note that *WordSmith* itself is a delightfully self-recursive part of its own portfolio!

