    .. start here...

############
Wander usage
############

.. |MapBox| raw:: html

   <a href="https://mapbox.com" target=_blank>MapBox</a>

.. |PSdoc| raw:: html

   <a href="https://positionstack.com/documentation" target=_blank>PositionStack API documentation</a>

.. |PositionStack| raw:: html

   <a href="https://positionstack.com" target=_blank>PositionStack</a>

.. |SwaggerHub| raw:: html

   <a href="https://swaggerhub.com" target=_blank>SwaggerHub</a>

.. |installed| raw:: html
            
   <a href="https://www.python.org/downloads/" target=_blank>must be installed</a>

.. |textsimilarity| raw:: html

   <a href="https://www.mdpi.com/2078-2489/11/9/421/htm" target=_blank>text similarity</a>

.. |GPSdistance| raw:: html

   <a href="http://www.movable-type.co.uk/scripts/latlong.html" target=_blank>GPS distance</a>

.. note::

   - to work with the underlying Python module, Python3 |installed| and accessible on the local OS!
   
   - a local dev environment is not mandatory, but if it is of interest, please go through the tutorial steps

There are two goals for this section:

  - *engage a Wander server*

    - information about three different (albeit closely related) techniques to invoke both the */forward* and */reverse* operations on a :ref:`Wander <outer-wander:the Wander tool>` server

  - *canonical task*

    - an expository demonstration of the canonical task mentioned in :ref:`outer-wander:the Wander tool`: use :ref:`Wander <outer-wander:the Wander tool>` to find a reliable lat/lon pair for a given ADDRESS, by going between the */forward* and */reverse* operations

.. tabs::

   .. tab:: engage a :ref:`Wander <outer-wander:the Wander tool>` server

      Three techniques are available for interactively engaging the :ref:`Wander <outer-wander:the Wander tool>` API behavior,:

         - *curl on the command line*
         
           - submit calls to the remote :ref:`Wander <outer-wander:the Wander tool>` server at :code:`http://cnxeng.pythonanywhere.com`
           - submit calls to a local :ref:`Wander <outer-wander:the Wander tool>` server at :code:`http://127.0.0.1:5000`

             .. note::

                to spin up a local :ref:`Wander <outer-wander:the Wander tool>` server, please visit the :ref:`tutorial/wander:Wander tutorial`
         
         - *invoke the wander.py methods directly:*
         
           - via a Python3 interpreter
           - via locally-written Python3 code

             .. note::
             
                - to work with the underlying Python module, Python3 |installed| and accessible on the local OS!
         
         - *SwaggerHub*
         
           - use it to verify the OpenAPI spec, and to execute the spec against the remote :ref:`Wander <outer-wander:the Wander tool>` server at :code:`http://cnxeng.pythonanywhere.com`

             .. note::

                the OpenAPI spec for :ref:`Wander <outer-wander:the Wander tool>` can be found in the :ref:`reference/wander:Wander API`

      For any of the three access techniques, the caller must choose a GEOSERVICE, and ensure there exists a valid GEOSERVICE_API_KEY. In the tabs below, the arbitrary choices are:

         .. code-block:: text

            export GEOSERVICE=positionstack

            export GEOSERVICE_API_KEY=<caller's API key>

      .. tabs::

         .. tab:: curl on the command line

            from a command line, use *curl* commands as follows:

            - first, choose a :ref:`Wander <outer-wander:the Wander tool>` server:

              - either *remote*::
            
                  export WANDER_SERVER=http://cnxeng.pythonanywhere.com

              - or *local*::

                  export WANDER_SERVER=http://127.0.0.1:5000

            - now utilize everything just defined, using example data from the |PSdoc|:

              .. tabs::

                 .. tab:: /foward

                    - invoke the '/forward' process on a known address ("1600 Pennsylvania Ave NW, Washington DC"), and specify that just one result should be returned:
                    
                      .. code-block:: bash
                         
                         curl --get --data-urlencode "api_key=${GEOSERVICE_API_KEY}" --data-urlencode "address=700 5th Ave, Seattle, WA 98104" --data "limit=1" ${WANDER_SERVER}/${GEOSERVICE}/forward
   
                      .. note::
                         *curl* requests to '/forward' use the **\-\-get** option so that *curl* handles urlencoding of 'address' values

                    - and observe the result:
                    
                      .. code-block:: json

                         {
                           "status": {
                             "code": 200, 
                             "reason": "OK"
                           }, 
                           "invoke": {
                             "geoservice": "positionstack", 
                             "direction": "forward", 
                             "address": "1600 Pennsylvania Ave NW, Washington DC"
                           }, 
                           "matches": [
                             {
                               "lat": 38.897675, 
                               "lon": -77.036547, 
                               "weight": 1, 
                               "callout": "1600 Pennsylvania Avenue NW, Washington, DC, USA"
                             }
                           ]
                         }

                 .. tab:: /reverse

                    - invoke the '/reverse' process on a known lat/lon pair, and specify that just one result should be returned:
                    
                      .. code-block:: bash
                      
                         curl "${WANDER_SERVER}/reverse?api_key=GEOSERVICE_API_KEY&lat=40.7638435&lon=-73.9729691&limit=1"
                    
                    - and observe the result:
                    
                      .. code-block:: json

                         {
                           "status": {
                             "code": 200, 
                             "reason": "OK"
                           }, 
                           "invoke": {
                             "geoservice": "positionstack", 
                             "direction": "reverse", 
                             "lat": 40.7638435, 
                             "lon": -73.9729691
                           }, 
                           "matches": [
                             {
                               "lat": 40.763841, 
                               "lon": -73.972972, 
                               "weight": 1, 
                               "callout": "Apple Store, New York, NY, USA"
                             }
                           ]
                         }

         .. tab:: invoke module methods

            to invoke underlying **wander.py** methods either from an interactive Python3 session or from locally-written Python3 code, follow these steps:

               .. note::
                  locally-written Python code will look the same as what the Python interpreter sees, so only the interpreter version is presented here

            - to use an interactive Python3 session, first capture a local copy of **wander.py**:

              - click the 'Copy' button in the top right corner of the code box in the *wander.py* tab
              - paste the clipboard contents into a local file named **wander.py**

            - now run the interpreter in the directory where **wander.py** is located:

              .. code-block:: bash
  
                 python3 -m venv --upgrade-deps ~/venv/py3-wander
                 source ~/venv/py3-wander/bin/activate
                 python3 -m pip install -U flask requests
                 python3

            - once inside the interpreter, execute the following lines of code:

              .. code-block:: python
              
                 import wander.py as w
              
                 api_key = GEOSERVICE_API_KEY
                 limit   = 1

                 address = "1600 Pennsylvania Ave NW, Washington DC"
              
                 w.geocode_forward ( api_key, address, limit, debug )

                 lat = 38.897675
                 lon = -77.036547
              
                 w.geocode_reverse ( api_key, lat, lon, limit, debug )
              
              .. note::
                 the results will be identical to those found in the previous tab: *curl on the command line*

         .. tab:: SwaggerHub

            #. in |SwaggerHub|, create a new project if necessary
            #. copy the code block in the **OpenAPI spec** tab
            #. select all of the content of the black square in the center of the page, delete it, then paste the clipboard content
            #. confirm the VALID tag appears at the bottom right of the black square
            #. on the right side of the page, click the line that starts with 'GET' and includes '/forward'
            #. click [Try it out]
            #. for values, choose:

               - geoservice : 'PositionStack'
               - api_key    : <a valid PositionStack API key>
               - address    : 1600 Pennsylvania Ave NW, Washington DC
               - limit      : 3
               - debug      : <leave empty>

            #. click [Execute]
            #. look in the 'Reponses' section for results
            #. ... and repeat as desired for the */reverse* operation

   .. tab:: canonical task

      The canonical task was introduced in the :ref:`Wander <outer-wander:the Wander tool>` section:

         "accept a (possibly vague) street address as an argument, and return both a standarized street address and a confirmed lat/lon (latitude/longitude) pair"

      To effect this, :ref:`Wander <outer-wander:the Wander tool>` suggests this processing sequence:

         #. use */forward* to map the ADDRESS to a list of matches, where each match contains a potential standardized address and its corresponding candidate lat/lon pair
         #. use */reverse* to map a selected candidate lat/lon pair to a new list of matches, where each match contains a potential standardized address and its corresponding candidate lat/lon pair
         #. evaluate the initial-vs-final congruence of both lat/lon pairs and address pairs:

            - for the lat/lon pairs:

              - measure the |GPSdistance| between the submitted lat/lon pair and the most recent lat/lon pair

            - for the address pairs:

              - measure the |textsimilarity| between the ADDRESS and the most recent potential standardized address

         #. adjust and repeat as needed

      The **/forward** and **/reverse** operations mentioned above are actually standard geo-location notions. When this information is returned by a full-scale GEOSERIVICE provider, there are many fields of information that come back from a given query (whether **forward** or **reverse**).

      For both operations, :ref:`Wander <outer-wander:the Wander tool>` works with the full calls to the GEOSERVICE. :ref:`Wander <outer-wander:the Wander tool>` then extracts the precise information it needs and returns that information in a Python 'dict' object. Please look at the tabs below for specific information for each operation.

      - the chosen *ADDRESS* is:
      
        - 700 5th Ave, Seattle, WA 98104

      .. note::
         - the weight value is provided by the GEOSERVICE, based on its internal processing

           - these values range between between 0.0 and 1.0, where 1.0 represents the highest confidence in the result

         - the 'callout' represents a form of a place name

           - it is derived by :ref:`Wander <outer-wander:the Wander tool>` from the GEOSERVICE response

      Details of the process are presented here:

      .. tabs::

         .. tab:: */forward*
   
            the primary argument for the */forward* search is ADDRESS; also, the results are limited to three matches:

               .. code-block:: text
               
                  - address: "700 5th Ave, Seattle, WA 98104"
                  - limit: 3

            and the function returns information that describes:

               .. code-block:: text

                  - the HTTP status
                    - status_code
                    - reason
                  - the calling values
                    - underlying GEOSERVICE
                    - which operation was invoked
                    - target address
                  - list of all search results, where each list entry comprises:
                    - lat
                    - lon
                    - weight
                    - callout

            the actual :ref:`Wander <outer-wander:the Wander tool>` return looks like this:

               .. code-block:: json
               
                  {
                    "status": {
                      "code": 200, 
                      "reason": "OK"
                    }, 
                    "invoke": {
                      "geoservice": "positionstack", 
                      "direction": "forward", 
                      "address": "700 5th Ave, Seattle, WA 98104"
                    }, 
                    "result": [
                      {
                        "lat": 47.604849, 
                        "lon": -122.329722, 
                        "weight": 1, 
                        "callout": "700 5th Avenue, Seattle, WA, USA"
                      }, 
                      {
                        "lat": 47.605098, 
                        "lon": -122.329921, 
                        "weight": 1, 
                        "callout": "700 5th Ave, Seattle, WA, USA"
                      }, 
                      {
                        "lat": 47.596441, 
                        "lon": -122.327486, 
                        "weight": 1, 
                        "callout": "700 5th Ave S, Seattle, WA, USA"
                      }
                    ]
                  }

         .. tab:: */reverse*
   
            the primary arguments for the */reverse* search are a lat/lon pair (extracted from the first return in the */forward* result); here, the results are limited to three matches:

               .. code-block:: text
               
                  - latitude: 47.6051
                  - longitude: -122.32992
                  - limit: 3

            and the function returns information that describes:

               .. code-block:: text

                  - the HTTP status
                    - status_code
                    - reason
                  - the calling values
                    - underlying GEOSERVICE
                    - which operation was invoked
                    - target lat
                    - target lon
                  - list of all search results, where each list entry comprises:
                    - lat
                    - lon
                    - weight
                    - callout

            the actual :ref:`Wander <outer-wander:the Wander tool>` return looks like this:

               .. code-block:: json

                  {
                    "status": {
                      "code": 200, 
                      "reason": "OK"
                    }, 
                    "invoke": {
                      "geoservice": "positionstack", 
                      "direction": "reverse", 
                      "lat": 47.6051, 
                      "lon": -122.32992
                    }, 
                    "result": [
                      {
                        "lat": 47.605098, 
                        "lon": -122.329921, 
                        "weight": 1, 
                        "callout": "700 5th Ave, Seattle, WA, USA"
                      }, 
                      {
                        "lat": 47.605066, 
                        "lon": -122.330036, 
                        "weight": 0.9, 
                        "callout": "700 5th Avenue, Seattle, WA, USA"
                      }, 
                      {
                        "lat": 47.605066, 
                        "lon": -122.330036, 
                        "weight": 0.9, 
                        "callout": "Seattle Presents, Seattle, WA, USA"
                      }
                    ]
                  }

         .. tab:: congruence

            note these characteristics:
            
               - the lat/lon pair from the */reverse* call is identically the same as what was submitted in the */forward* call
            
                 - note: this is not true of the second lat/lon pair from the cll to */revers*
            
               - the weight from the */reverse* call is 1
            
               - the 'callout' value from the */reverse* call is identically the same as the 'callout' value from the */forward* call, and is therefore a worthy potential standardized street address
            
                 - note: this is not true of the second result's 'callout' value
            
               - the potential standardized street address ("700 5th Ave, Seattle, WA, USA") differs only slightly -- and in well-known ways -- from the initial ADDRESS ("700 5th Ave, Seattle, WA 98104")

         .. tab:: conclusion

            - the initial ADDRESS "700 5th Ave, Seattle, WA 98104" is well represented by the lat/lon pair "47.6051/-122.32992", and *vice versa*
            - therefore, there is no need to adjust any steps and repeat the entire process
