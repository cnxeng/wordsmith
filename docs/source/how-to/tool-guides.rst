.. start here...
   first: hand-off guide for a Java/Selenium BVT tool used in a Jenkins workflow
   second: User's Guide for a custom real-time test tool for a multi-processing embedded system

###########
Tool guides
###########

.. |BVT| raw:: html

   <a href="../_static/confluence-clean.html" target=_blank>BVT tool</a>

.. |csp| raw:: html

   <a href="https://en.wikipedia.org/wiki/Communicating_sequential_processes" target=_blank>Tony Hoare's pioneering work</a>

.. |RT| raw:: html

   <a href="../_static/LogWatch.pdf" target=_blank>RT monitoring tool</a>


BVT for Jenjins
===============

   - hand-off guide for a Java/Selenium BVT tool used in a Jenkins workflow
   - tool is used as a gating phase for a complex CI/CD testing package that analyzes an enterprise-level CDM system

      - this author created the testing package, the BVT tool, and the documentation, and then maintained them until they were all handed off to a 3rd party

   - |BVT|

RT monitoring
=============
      
   - User's Guide for a real-time, event-driven embedded environment with four independant and communicating sequential processes (remember |csp|?)
      
      - this author created and maintained both the tool and the documentation before being rolled off to another assignment

   - |RT|
