    .. start here...

##########
Wander API
##########

.. |MBdoc| raw:: html

   <a href="https://docs.mapbox.com/api/search/geocoding/#geocoding-response-object" target=_blank>MapBox API documentation</a>

.. |MapBox| raw:: html

   <a href="https://mapbox.com" target=_blank>MapBox</a>

.. |SwaggerHub| raw:: html

   <a href="https://swaggerhub.com" target=_blank>SwaggerHub</a>

.. |installed| raw:: html
            
   <a href="https://www.python.org/downloads/" target=_blank>must be installed</a>

Wander presents a webservice specified by an OpenAPI specification, and also the underlying Python module.

   .. note::

      - both the OpenAPI specification and the underlying Python module were created specifically for the *WordSmith* portfolio project
      - to work with the underlying Python module, Python3 |installed| on the local OS!

To support *WordSmith*'s use of the Wander API to demonstrate the *reference* kind of technical documentation, this topic presents two varieties of reference material:

   - the OpenAPI specification that is the foundation for the Wander API behavior
   - docstrings written for the supporting Python module

.. toctree::
   :maxdepth: 2

Inside
======

.. tabs::

   .. tab:: OpenAPI spec

      .. note::
         An OpenAPI spec can be quite minimal, but that does not provide very much useful information. There is a lot of value in having the spec carefully fleshed out.

      .. literalinclude:: /_static/openapi.yaml
         :language: yaml

   .. tab:: module reference

      .. note::
         - This *docstring* information is captured automatically by Sphinx, but it is often the technical writer's responsibility to work with the tech team to ensure that the *docstring* contents are coherent and complete in the first place.
         - (there is an MIT license in the module)

      .. automodule:: wander
         :members:
