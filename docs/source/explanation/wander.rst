.. start here...

##############
Wander context
##############

.. |MBdoc| raw:: html

   <a href="https://docs.mapbox.com/api/search/geocoding/#geocoding-response-object" target=_blank>MapBox API documentation</a>

.. |PSdoc| raw:: html

   <a href="https://positionstack.com/documentation" target=_blank>PositionStack API documentation</a>

.. |PositionStack| raw:: html

   <a href="https://positionstack.com" target=_blank>PositionStack</a>

The underlying concept for the :ref:`Wander <outer-wander:the Wander tool>` tool is to be a very thin and very precise layer over calls to a web-based GEOSERVICE. There are three primary aspects at work:

#. :ref:`Wander <outer-wander:the Wander tool>` is accessible in two ways:

   - via HTTP calls to a Web URL (that implements an OpenAPI spec)
   - via direct invocation of methods in the supporting Python module

#. :ref:`Wander <outer-wander:the Wander tool>` implements two precise and restricted operations that are modelled directly after two corresponding and standard GEOSERVICE navigation operations:

   - **/forward**

     - accepts: a street address
     - returns: a list of weighted matches, where each match consists of:

       - lat
       - lon
       - weight (0 to 1, from the GEOSERVICE)
       - callout (standardized address extracted from the GEOSERVICE)

   - **/reverse**

     - accepts: a lat/lon pair
     - returns: a list of weighted matches, where each match consists of:

       - lat
       - lon
       - weight (0 to 1, from the GEOSERVICE)
       - callout (standardized address extracted from the GEOSERVICE)

   .. note::
      - by design, the returned information from both operations has the same structure
      - the underlying GEOSERVICE calls themselves return extensive amounts of information that :ref:`Wander <outer-wander:the Wander tool>` does not preserve
      - for example: when using |PositionStack|, Wander extracts the 'latitude', 'longitude', 'confidence', and 'label' fields from the |PositionStack| return

        - compare Wander results with what is found in the |PSdoc|:

         .. tabs::

            .. tab:: /forward

               .. tabs::

                  .. tab:: target

                     - address = "1600 Pennsylvania Ave NW, Washington DC"

                  .. tab:: result - PositionStack

                     .. code-block:: json

                        {
                           "latitude": 38.897675,
                           "longitude": -77.036547,
                           "label": "1600 Pennsylvania Avenue NW, Washington, DC, USA",
                           "name": "1600 Pennsylvania Avenue NW",
                           "type": "address",
                           "number": "1600",
                           "street": "Pennsylvania Avenue NW",
                           "postal_code": "20500",
                           "confidence": 1,
                           "region": "District of Columbia",
                           "region_code": "DC",
                           "administrative_area": null,
                           "neighbourhood": "White House Grounds",
                           "country": "United States",
                           "country_code": "US",
                           "map_url": "http://map.positionstack.com/38.897675,-77.036547"
                        }

                  .. tab:: result - Wander

                     .. code-block:: json

                        {
                          "lat": 38.897675, 
                          "lon": -77.036547, 
                          "weight": 1, 
                          "callout": "1600 Pennsylvania Avenue NW, Washington, DC, USA"
                        }

            .. tab:: /reverse

               .. tabs::

                  .. tab:: target

                     - lat = 40.763841

                     - lon = -73.972972

                  .. tab:: result - PositionStack

                     .. code-block:: json

                        {
                           "latitude": 40.763841,
                           "longitude": -73.972972,
                           "label": "Apple Store, Manhattan, New York, NY, USA",   
                           "name": "Apple Store",
                           "type": "venue",
                           "distance": 0,
                           "number": "767",
                           "street": "5th Avenue",
                           "postal_code": "10153",
                           "confidence": 1,
                           "region": "New York",
                           "region_code": "NY",
                           "administrative_area": null,
                           "neighbourhood": "Midtown East",
                           "country": "United States",
                           "country_code": "US",
                           "map_url": "http://map.positionstack.com/40.763841,-73.972972",
                        }

                  .. tab:: result - Wander

                     .. code-block:: json

                        {
                          "lat": 40.763841, 
                          "lon": -73.972972, 
                          "weight": 1, 
                          "callout": "Apple Store, New York, NY, USA"
                        }

#. :ref:`Wander <outer-wander:the Wander tool>` is immediately useful for the canonical task, where the analyst follows this cycle:

   - invoke the **/forward** operation with a (possibly vague) street address
   - choose a lat/lon pair from the results, and supply them to the **/reverse** operation
   - compare the resulting standardized address to the inital street address
   - adjust and repeat as needed

Once the caller is satisfied with both a standardized address and a lat/lon pair, the canonical task is complete. Moreover, this information can subsequently be used to acquire comprehensive information directly from the GEOSERVICE (albeit outside of :ref:`Wander <outer-wander:the Wander tool>`).

These discussion items are explicitly examined in the :ref:`how-to/wander:Wander usage` section.
