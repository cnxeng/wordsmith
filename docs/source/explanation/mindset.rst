
################
Mindset articles
################

.. |symbiosis| raw:: html

   <a href="https://docs.google.com/document/d/1cksO1dxcqjQlI4XeBNgOTx1TeENDYf7It9Vhj_w6W_8/edit?usp=sharing" target=_blank>The Symbiosis Behind Good Software</a>

.. |s2| raw::  html

   <a href="https://docs.google.com/document/d/1jnx_2ghH1cJR9Vc9OdiEdpl8ROyCcieEfISIXJIPUqc/edit" target=_blank>Symbiosis and Good Software</a>

.. |Requirements| raw:: html

   <a href="https://docs.google.com/document/d/1R7JMLbzR7FVFkARLPCThmK8lgvBdiO8aNhGBDweh_TA/preview" target=_blank>Requirements are Everything</a>

.. |321+| raw:: html

   <a href="https://docs.google.com/document/d/1Znz9echcTRj82JZQh0fE20CtN_-Qook9iH0u1FHbLX4/preview" target=_blank>'321+' is How the Software World Unfolds <em>(note: work in progress!)</em></a>

.. |TestFrame| raw:: html

   <a href="https://docs.google.com/document/d/1A2EEfzw8oC8LkFcvvn4YJGmBAJfbFFPlsQW6lHKp_LQ/preview" target=_blank>High-Level View of the Very Best Software Test Framework I Ever Created</a>

An on-going collection of personal perspectives from 40+ years of software as a career.

- |Requirements|
- |TestFrame|
- |321+|

