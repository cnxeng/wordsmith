    .. start here

#############
Basis project
#############

Overview - Basis
================

The end goal of this topic is to define how to create a new Python3-based Sphinx documentation project, and how to equip it with a well-defined, Python3-based infrastructure for development and maintenance purposes.

.. |installed| raw:: html
            
   <a href="https://www.python.org/downloads/" target=_blank >installed</a>

.. note::

   - because the Sphinx tools and the Basis project all use Python3, Python3 *must* be |installed| on the local OS!

When built out, the infrastructure will support a standardized workflow with these three characteristics:

   #. documentation content is developed incrementally and iteratively in a local environmet
   #. when appropriate, the local content is published to the Web for global consumption
   #. these two steps are repeated as often as needed

The underlying Sphinx project is built in a standard fashion, the infrastructure is fashioned from typical elements, and the end-goal workflow looks like this in pseudo-code:

.. code-block:: text
 
   loop forever and a day:
      until ready to publish...
         loop
            edit local files
            save changes
            review in the local autobuild browser
         endloop
      enduntil
     
      when ready to publish...
         push the project to the GitLab repository
         review the published docs
      endwhen
   endloop

In order to construct a project and an infrastructure that directly support this end-goal workflow, we will walk step-by-step through the details of the following high-level sequence of activities:

.. code-block:: none
        
   1) create a new GitLab project named 'Basis'
   2) create a new local Sphinx project named 'Basis'
      a) create and initialize a local directory named 'Basis'
      b) prepare the initial Sphinx setup
      c) modify the file 'docs/source/conf.py'
      d) modify the file 'docs/source/index.rst'
   3) connect the local Sphinx project to the GitLab project
   4) establish a local autobuild environment for incremental development
   5) as appropriate, trigger a CI/CD workflow that publishes to the Web

.. note::

   - the upcoming series of **Tutorial Steps** presumes the user has an introductory level of familiarity with the following areas:
   
   .. tabs::

      .. tab:: basic git workflow

         - init
         - status
         - add
         - commit
         - push
         - remote

      .. tab:: fundamental GitLab
      
         - access GitLab
         - create a project
         - do basic navigation

      .. tab:: essential bash
      
         - such as Linux, MacOS, cygwin, Git Bash
         - basic navigation
         - basic shell commands (cd, mkdir, less, etc.)
         - executing commands

      .. tab:: plain text editor
      
         .. note::

            this must be a plain text editor - not HTML, PDF, or word processing

            - emacs, vim, notepad++, kate, sublime, jedit, etc.

         - open the editor
         - access a file
         - make changes to the file's contents
         - save the file
         - close the editor


Tutorial Steps
==============

The following set of tabs implements the sequence from above -- please follow the tabs in order!

   #. create a new GitLab project named 'Basis'
   #. create a new local Sphinx project named 'Basis'
   #. connect the local Sphinx project to the GitLab project
   #. establish a local autobuild environment for incremental development
   #. as appropriate, trigger the CI/CD workflow that publishes to the Web

.. tabs::

   .. tab:: [1] GitLab

      - log in to GitLab and create a Basis project

        .. note::
  
           The table below employs *WordSmith*'s text-based convention for specifying navigation actions on a web page:
  
           - **<action>**
  
             - bold initial words in angle brackets '**<...>**' represent a UI action the user should take
  
           - *[target]*
  
             - italicized terms in square brackets '*[...]*' represent interactive items on the page (buttons, menu items, links, etc.)
             - an interactive item with a leading angle bracket '*[> ...]*' represents sub-item, such as a menu pick
  
           - specifics
  
             - plain words on the web page that...
  
               - are entered by the user (e.g., credentials)
               - or are generally non-interactive text (descriptive text, non-interactive labels, etc.)
  
           - (comments)
  
             - words in parentheses are guidance for the user
  
        ====================== ===================================== ===================== ==========
        **<action>**           *[target]*                            specifics             (comments)
        ====================== ===================================== ===================== ==========
        **<log in>**           *[https://gitlab.com/]*               username, password    (user's GitLab account)
        **<click>**            *[Menu]*                                                    (near the GitLab icon at
        
                                                                                           the left of the title bar)
        **<click>**            *[Groups]*                            
        **<navigate>**                                                                     (to the appropriate group)
        **<click>**            *[New project]*                                             (top right corner of the page)
        **<click>**            *[Create blank project]*              
        **<enter>**            *[Project name]:*                     Basis
        **<choose>**           *[Visibility Level]:*                 Private
        **<navigate>**         *[Project Configuration]*
        **<uncheck>**          *[> Initiate ...  README]*
        **<uncheck>**          *[> Enable ... (SAST)]*
        **<click>**            *[Create project]*
        **<click>**            *[Clone]*
        **<navigate>**         *[Clone with HTTPS]* 
        **<click>**            *[> clipboard icon]* 
        ====================== ===================================== ===================== ==========
  
        .. note::
           - in the final step above -- i.e., **<click>** *[> clipboard icon]* -- the capture to the clipboard will be in the form *git@gitlab.com:<group-name>/basis.git*
           - store this in an environment variable:
        
           .. code-block:: bash
        
              export GIT_REMOTE='<paste from clipboard>'

      - **now, please continue to the '[2] Sphinx' tab**

   .. tab:: [2] Sphinx

      .. |venv| raw:: html

         <a href="https://www.docslikecode.com/learn/01-sphinx-python-rtd/" target=_blank>Python Virtual Environment</a>

      - prepare and modify a local Sphinx project by following these steps:

        - prepare a local directory named 'Basis', including a |venv|
        - equip the 'Basis' directory as a Sphinx project
        - modify the configuration file 'Basis/docs/source/conf.py'
        - modify the initial file 'Basis/docs/source/index.rst'

      .. note::

         The local 'Basis' directory can be created anywhere convenient. To be specific, its parent directory does not need any particular name.

      .. tabs::

         .. tab:: [a] local Basis dir

            - cd to the directory where the Basis project is to be created

            - then execute these lines:

              .. code-block:: bash
              
                 mkdir ./Basis
                 cd ./Basis
                 mkdir docs
  
                 git init -b main
              
                 echo ''             >  .gitignore
                 echo 'build/'       >> .gitignore
                 echo '__pycache__/' >> .gitignore
                 echo ''             >> .gitignore
                 echo '*~'           >> .gitignore
                 echo ''             >> .gitignore
                 echo 'Thumbs.db'    >> .gitignore
                 echo '.DS_Store'    >> .gitignore
                 echo 'nul'          >> .gitignore
                 echo ''             >> .gitignore
              
                 echo ''                                          >  .gitlab-ci.yml
                 echo 'image: python:3.10-alpine'                 >> .gitlab-ci.yml
                 echo ''                                          >> .gitlab-ci.yml
                 echo 'pages:'                                    >> .gitlab-ci.yml
                 echo '  stage: deploy'                           >> .gitlab-ci.yml
                 echo '  script:'                                 >> .gitlab-ci.yml
                 echo '  - pip install -U sphinx'                 >> .gitlab-ci.yml
                 echo '  - pip install -U -r requirements.txt'    >> .gitlab-ci.yml
                 echo '  - sphinx-build -b html -W source public' >> .gitlab-ci.yml
                 echo '  artifacts:'                              >> .gitlab-ci.yml
                 echo '    paths:'                                >> .gitlab-ci.yml
                 echo '    - public'                              >> .gitlab-ci.yml
                 echo '  only:'                                   >> .gitlab-ci.yml
                 echo '  - main'                                  >> .gitlab-ci.yml
                 echo ''                                          >> .gitlab-ci.yml
  
                 echo 'sphinx'                 >  requirements.txt
                 echo 'sphinx-rtd-theme'       >> requirements.txt
                 echo 'sphinx-copybutton'      >> requirements.txt
                 echo 'sphinx-tabs'            >> requirements.txt
                 echo 'sphinxcontrib-napoleon' >> requirements.txt
                 echo 'sphinx-autobuild'       >> requirements.txt
                 echo 'pyyaml'                 >> requirements.txt
                 echo 'flask'                  >> requirements.txt
  
                 python3 -m venv --upgrade-deps ~/venv/py3-sphinx
                 source ~/venv/py3-sphinx/bin/activate
                 python3 -m pip install -U -r requirements.txt

            - **now, please continue to the '[b] equip for Sphinx' tab**

         .. tab:: [b] equip for Sphinx

            - in the 'Basis' directory, execute these commands:

            .. code-block:: bash
            
               source ~/venv/py3-sphinx/bin/activate
            
               sphinx-quickstart     \
               --sep                 \
               --project  Basis      \
               --author  'Your Name' \
               --release  0.0.1      \
               --language en         \
               --ext-autodoc         \
               --ext-doctest         \
               --ext-intersphinx     \
               --ext-todo            \
               --ext-coverage        \
               --ext-imgmath         \
               --ext-mathjax         \
               --ext-ifconfig        \
               --ext-viewcode        \
               --ext-githubpages     \
               --extensions sphinx_copybutton,sphinx_tabs.tabs,sphinx.ext.napoleon,sphinx.ext.autosectionlabel \
               docs
            
               echo '#####'                      >  ./docs/source/basis.rst
               echo 'Basis'                      >> ./docs/source/basis.rst
               echo '#####'                      >> ./docs/source/basis.rst
               echo ''                           >> ./docs/source/basis.rst
               echo 'this is your Basis content' >> ./docs/source/basis.rst
               echo ''                           >> ./docs/source/basis.rst
            
               touch ./docs/source/_static/.gitkeep
               touch ./docs/source/_templates/.gitkeep

               python3 -m sphinx -b html -W ./docs/source ./docs/source/build/html

            - **now, please continue to the '[c] conf.py' tab**

         .. tab:: [c] conf.py

            - open the file './docs/source/conf.py' in any text editor
            
              - first, locate these lines:
              
              .. code-block::
              
                 copyright = '2022, <Your Name>'
                 author = '<Your Name>'
              
              - and modify them appropriately
              
              - next, locate this line:
              
                  ``exclude_patterns = []``
              
              - and modify it to be this:
              
                  ``exclude_patterns = [ 'build', 'Thumbs.db', '.DS_Store', 'nul' ]``
              
              - finally, locate this line:
              
                    ``html_theme = 'alabaster'``
              
              - and replace it with these three lines::
              
                    import sphinx_rtd_theme
                    html_theme = 'sphinx_rtd_theme'
                    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
            
            - save the file
            
            - close the editor

            - **now, please continue to the '[d] index.rst' tab**

         .. tab:: [d] index.rst

            - open the file 'Basis/docs/source/index.rst' in any text editor
            
              - locate this line:
              
                   ``:caption: Contents:``
              
              - insert a blank line
                    
              - insert the line:
              
                 ``basis.rst``
              
              - adjust the indentation on this line
              
                - it needs three leading spaces
            
            - save the file
            - close the editor
            - **now, please continue to the '[3] connect' tab**

   .. tab:: [3] connect

      - in the 'Basis' directory, execute these lines:
      
      ..  code-block:: bash
      
         git add .
         git commit -m "Initial commit"
         git remote add origin $GIT_REMOTE
         git push --set-upstream origin main

      - **now, please continue to the '[4] autobuild` tab**

   .. tab:: [4] autobuild

      - in a *new* terminal opened in the 'Basis' directory, execute these lines::
      
          source ~/venv/py3-sphinx/bin/activate

          python3 -m sphinx_autobuild -a -b html -W --watch . --open-browser --port=0 --re-ignore /.*~/ ./docs/source ./docs/source/build/html

      - this will open a new browser
      
      - do not kill the autoloader terminal!
      
      - find the browser window that was opened
      
      - keep it open and available

      - **now, please continue to the '[5] publish' tab**

   .. tab:: [5] publish

      - in the 'Basis' directory, execute these lines to trigger the automatic publishing of the project:
      
        ..  code-block:: bash
            
            git add .
            git commit -m "<commit message>"
            git push
        
      - monitor the GitLab project for a successful build (click the ``CI/CD configuration`` button)
      - open/refresh a browser at ``https://<group name>.gitlab.io/basis``
      
      .. note::
         to  confirm this address:

         - open GitLab
         - navigate to Project > Settings > Pages
         - look near the very bottom

Conclusion
==========

Congratulations! The Basis project and its associated infrastructure are now ready to use!

Quick Review
------------

What has been accomplished:

- both the Basis project and the supporting infrastructure have been instantiated

- this has enabled a specific high-level workflow:

  - develop locally and incrementally (edit, review, repeat)
  - publish globally (review, enhance)
  - repeat as needed
  - and use automation as much as possible!

Next steps
----------

Here are some observations about ways to work with the Basis and the infrastructure:

- expand, extend, and enhance the project's contents

  - just what Sphinx is here for!

- modify 'Basis' to some other name

  - copy the code blocks to a working file, change '(B/b)asis' to something else, then execute the modified blocks 

  - be sure to create the new GitLab project!
    
- change how Basis is built

  - alter what is written into ``Basis/requirements.txt``
  - change the options used when calling ``sphinx-quickstart``

  .. note::

     For the ``sphinx-quickstart`` invocation, the *WordSmith* approach is to pull in every extension that might be used, so that development doesn't have to pause and make adjustments when a new feature is to be engaged. However, many professionals prefer instead to initially bring in a minimal set of extensions, and then expand as needed. Both approaches have merit, it comes down to individual (or management!) preference.

- engage different environment elements:

.. tabs::

   .. tab:: markup language

      - markdown

        Sphinx will also process markdown, so that is a viable choice. However, there are reasons for choosing reST over markdown, such as those described described |here|.

      .. |here| raw:: html

         <a href="https://eli.thegreenplace.net/2017/restructuredtext-vs-markdown-for-technical-documentation/" target=_blank>here</a>

   .. tab:: infrastructure

      The project repository could easily be hosted on other services, such as:

      - GitHub
      - BitBucket

      Both of these offer full git functionality, and CI/CD workflows as well. However, *WordSmith* has found the current workflow using GitLab to be the most effective for its purposes.

   .. tab:: web publishing

      - this could also be done using ReadTheDocs (RTD)

      .. note::
          an early implementation of *WordSmith* had problems with RTD and happily uses GitLab Pages, but if RTD is desired, two things must happen:
      
            - create an RTD account and link it to the Basis project in GitLab
            - replace the ``.gitlab-ci.yml`` script block with this::
          
                echo ''                                     >  .readthedocs.yaml
                echo 'version: 2'                           >> .readthedocs.yaml
                echo ''                                     >> .readthedocs.yaml
                echo 'sphinx:'                              >> .readthedocs.yaml
                echo '  builder: html'                      >> .readthedocs.yaml
                echo '  configuration: source/conf.py'      >> .readthedocs.yaml
                echo '  fail_on_warning: true'              >> .readthedocs.yaml
                echo ''                                     >> .readthedocs.yaml
                echo 'python:'                              >> .readthedocs.yaml
                echo '  install:'                           >> .readthedocs.yaml
                echo '    - requirements: requirements.txt' >> .readthedocs.yaml
                echo ''                                     >> .readthedocs.yaml

Final thought
-------------

There is an enormous amount of material out on the web about both technical writing in general, and Sphinx+reST in particular. A functioning Basis project enables the active, hands-on learning of both!
