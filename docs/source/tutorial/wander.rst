.. start here...

.. |installed| raw:: html
            
   <a href="https://www.python.org/downloads/" target=_blank>must be installed</a>

###############
Wander tutorial
###############

The primary goal of the :ref:`Wander <outer-wander:the Wander tool>` tool is to access a remote **Wander** server. However, it is entirely possible to study the :ref:`Wander <outer-wander:the Wander tool>` API behavior using a local server. This local server is straightforward to set up using a simple Python approach: it comprises nothing more than a Flask app that is launched via the **wander.py** file.

Why do this? Because using a local server offers two specific capabilities not possible with the remote server:

  - debugging output can be viewed directly in the local server's log entries (not viewable when using the remote srver)
  - modifications can be made to the local **wander.py** content (not accessible on the remote server)

And how all this comes about is wonderfully uncomplicated, as explained in the *local dev environment* tab below.

      .. note::

         - to follow these steps, Python3 |installed| and accessible on the local OS!

.. tabs::                  

   .. tab:: local dev environment

      Here is how to establish and use a local :ref:`Wander <outer-wander:the Wander tool>` server:
   
        - first, ensure the following has been run at least once on the command line (and don't worry, running it more than once causes no problems!)::
   
            python3 -m pip install requests flask
       
        - next, set up the local **wander.py** file:

          - click the 'Copy' button in the top right corner of the code box in the *wander.py* tab
          - paste the clipboard contents into a local file named **wander.py**
    
        - finally, open a new Terminal in the directory where **wander.py** was created, and run these commands:

          .. code-block:: python
    
             python3 -m venv --upgrade-deps ~/venv/py3-wander
             source ~/venv/py3-wander/bin/activate
             python3 -m pip install -U flask requests
             python3 wander.py

          - this launches a local Flask app which uses the local **wander.py** to respond to HTTP requests at http://127.0.0.1:5000
          - also, the local server session shows debugging output whenever ``&debug`` is added to any request
          - **do not** close the Terminal until the dev session is complete!

      And now it is possible to submit HTTP requests to the local server address by browser or by curl, as explained in the next tab *engagement*

   .. tab:: wander.py
   
      .. literalinclude:: ../../../wander.py
         :language: python
