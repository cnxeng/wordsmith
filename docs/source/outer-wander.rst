


###############
the Wander tool
###############

.. |PositionStack| raw:: html

   <a href="https://positionstack.com" target=_blank>PositionStack</a>

.. |MapBox| raw:: html

   <a href="https://mapbox.com" target=_blank>MapBox</a>

.. |geopositioning| raw:: html

   <a href="https://en.wikipedia.org/wiki/Geopositioning" target=_blank>here</a>

   

.. toctree::
   :maxdepth: 2
   :hidden:

   tutorial/wander
   how-to/wander
   reference/wander
   explanation/wander

As one part of this portfolio, *WordSmith* has created a small Python-based web service called **Wander**. It is a very thin and very precise geolocation tool that is built as an API layer over calls to a web-based GEOSERVICE.

For a gentle introduction to geolocation, please look |geopositioning|.

The tool's corresponding :ref:`background/4kinds:4 kinds of docs` are presented in the following set of four tabs:

   .. tabs::

      .. tab:: tutorial

         The primary goal of the :ref:`Wander <outer-wander:the Wander tool>` tool is to access a remote **Wander** server. However, it is entirely possible to study the :ref:`Wander <outer-wander:the Wander tool>` API behavior using a local server. This local server is straightforward to set up using a simple Python approach: it comprises a Flask app that is launched via the **wander.py** file.

         How this comes about is examined in the step-by-step instructions found here:

           - :ref:`tutorial/wander:Wander tutorial`

      .. tab:: how-to

         The *tutorial* tab (first tab on the left) shows how to set up a local dev environment. The **Wander** tool is introduced in a general way in the *explanation* tab (first tab on the right).

         With all that in mind, here is information describing how to engage the tool and make calls to it:
     
           - :ref:`how-to/wander:Wander usage`

      .. tab:: reference
         
         Web services and the software that drives them are both topics that need solid reference guides. For Wander, these comprise:
         
            - an *OpenAPI specification* for the web service
            - a *programmer's reference* for the underlying Python module (used by Flask)
         
         For detailed technical reference information, please look here:

          - :ref:`reference/wander:Wander API`
   
      .. tab:: explanation

         **Wander** is a (very!) small tool that comprises a thin and tightly-focussed layer over a comprehensive API from a geo-location service. The tool is highly specific in its intent:

           - provide two operations (*/forward*, */reverse*) that assimilate and minify the two corresponding operations from a user-chosen, full-scale, global-level geo-location service (currently either |PositionStack| or |MapBox|)

         Plus, there is a canonical workflow task that the **Wander** tool is designed to support:

           - accept a (possibly vague) street address as an argument, and return both a standarized street address and a confirmed lat/lon (latitude/longitude) pair

         For detailed information, please continue reading for, respectively, first a general overview of geolocation services, and then more precise description of **Wander** accomplishes:

           - :ref:`explanation/wander:Wander context`
