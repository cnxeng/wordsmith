


###############
the Legacy docs
###############

.. toctree::
   :maxdepth: 2
   :hidden:

   tutorial/basis
   how-to/tool-guides
   explanation/mindset

These are disparate, individual documents gathered from several different projects over many years. They are not related, but instead assembled as a collection of other examples of the :ref:`background/4kinds:4 kinds of docs`.

   .. tabs::

      .. tab:: tutorial

         Here, the *WordSmith* portfolio presents step-by-step instructions for building out what it calls the 'Basis' project, which is *WordSmith*'s :ref:`background/sphinx+rest:rST/Sphinx` starting point (in fact, it was used to create *WordSmith* itself!).
     
         - :ref:`tutorial/basis:Basis project`

      .. tab:: how-to

         The two documents included here are (appropriately redacted) legacy items from previous projects, written by this author as part of software tools developed for those projects.
         
         - :ref:`how-to/tool-guides:Tool guides`

      .. tab:: reference

         .. warning::
            Due to extensive proprietary content, no Legacy 'reference' items are presented.
   
      .. tab:: explanation
   
         Pulling discussion documents from previous projects would involve so much scrubbing to remove proprietary information, that the discussions would not make much sense. Instead, *WordSmith* presents software-related writing from this author's personal articles. Additional articles are always in progress and will be added as they materialize.
   
         - :ref:`explanation/mindset:Mindset articles`
