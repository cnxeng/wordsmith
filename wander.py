

"""
Copyright 2021 Sean Lanter

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


import json
import yaml
import pprint

import requests
import flask


# utility method for debugging
def pp ( obj="", apply_sort=False ):
   pprint.pprint ( obj, indent=3, width=132, sort_dicts=apply_sort )
   

# sanitize the 'geoservice' path parameter
def _scrub ( geoservice ):
   if geoservice.lower() in [ "mapbox", "mb" ]:
      geoservice = "mapbox"
   elif geoservice.lower() in [ "positionstack", "ps" ]:
      geoservice = "positionstack"
   else:
      geoservice = ""

   return geoservice


# create and configure the Flask app
app = flask.Flask ( __name__ )
app.config['JSON_SORT_KEYS'] = False


@app.route ( "/<geoservice>/forward" )
def flask_geocode_forward ( geoservice ):
   """address -> lat/lon

   Mapping layer between the web and geocode_forward().

   Returns:
      result of calling geocode_forward() with parameters extracted from flask.request.args
   """

   ans = ""

# required

   geoservice = _scrub ( geoservice )

   api_key = flask.request.args.get ( 'api_key' )
   address = flask.request.args.get ( 'address' )

# optional

   limit = int ( flask.request.args.get ( 'limit', 3 ) )
   debug = flask.request.args.get ( 'debug' ) is not None

   if not geoservice:
      ans = ( "Bad Request -> missing required path parameter: 'geoservice'", 400 )

   elif api_key is None:
      ans = ( "Bad Request -> missing required query parameter: 'api_key'", 400 )

   elif address is None:
      ans = ( "Bad Request -> missing required query parameter: 'address'", 400 )

   elif limit < 1:
      ans = ( f"Bad Request -> value ({limit}) of query parameter 'limit' is below its minimum (1)", 400 )

   else:
      ans = geocode_forward ( geoservice, api_key, address, limit, debug )

   if debug or isinstance ( ans, tuple ):
      print()
      print ( ">>>>>>>>>>>>>>>>>>>>>>>>>" )
      pp    ( geoservice )
      pp    ( flask.request.args.keys() )
      print ( "-------------------------" )
      print ( ans )
      print ( "<<<<<<<<<<<<<<<<<<<<<<<<<" )
      print()

   return ans


def geocode_forward ( geoservice, api_key, address, limit, debug=False ):
   """address -> lat/lon

   Args:
     geoservice (str) : underlying geoservice provider
     api_key (str)    : caller's geoservice API token
     addresss (str)   : street address, such as "700 5th Ave, Seattle, WA 98104"
     limit (int)      : number of lat/lon pairs to return
     debug (bool)     : print out debug info?

   Returns:
     dict: { 'status': { 'code':http-status-code, 'reason':http-reason }, 'invoke': { 'geoservice':geoservice, 'direction':"forward", 'address':address }, 'matches': [ { 'lat':latitude, 'lon':longitude, 'weight':weight, 'callout':callout }, ... ] }

   The essential "forward geo-locate" information returned is what the Wander API module has chosen to be of primary importance. It consists of a list of 'dict' instances built from four fields in the individual entries in the query response, one instance per entry:

     - latitude   : of the located resource
     - longitude  : of the located resource
     - weight     : between 0 and 1 (inclusive), where 1 is highest
     - callout    : the common name for this address

   This list of 'dict' instances is, in turn, assembled into an outer 'dict' instance that also includes the HTTP status information of the underlying MapBox request, plus the core information from the calling sequence.
   """

   _remaining      = 2
   _status_code_ok = True
   gather          = [ {} ]

   if geoservice == "mapbox":
      params = {
         'access_token' : api_key,
         'limit'        : limit,
      }
      endpoint = f"https://api.mapbox.com/geocoding/v5/mapbox.places/{address}.json"
   elif geoservice == "positionstack":
      params = {
         'access_key': api_key,
         'query'     : address,
         'limit'     : limit,
      }
      endpoint = "http://api.positionstack.com/v1/forward"
   else:
      params = {}
      endpoint = ""

   while _remaining > 0 and not gather[0] and _status_code_ok:

      r = requests.get ( endpoint, params=params )

      if debug:
         print()
         print ( f">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> // {_remaining}" )
         print ( f"status_code={r.status_code} reason={r.reason}" )
         print ( "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" )
         pp ( params )

      if r.status_code == 200:
         if geoservice == "mapbox":
            info   = r.json()['features']
            gather = [ { 'lat':_['center'][1], 'lon':_['center'][0], 'weight':_['relevance'], 'callout':_['place_name'] } if _  else {} for _ in info ]
         elif geoservice == "positionstack":
            info   = r.json()['data']
            gather = [ { 'lat':_['latitude'], 'lon':_['longitude'], 'weight':_['confidence'], 'callout':_['label'] } if _  else {} for _ in info ]
         else:
            info   = ""
            gather = []

         if debug:
            print ( "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" )
            pp ( info )
         else:
            pass

         _remaining -= 1

      else:
         _status_code_ok = False

      if debug:
         print ( "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" )
         pp ( gather )
         print ( "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" )
         print()

   ans = {
      'status': { 'code':r.status_code, 'reason':r.reason },
      'invoke': { 'geoservice':geoservice, 'direction':"forward", 'address':address },
      'matches': gather
   }

   return ans


@app.route ( "/<geoservice>/reverse" )
def flask_geocode_reverse ( geoservice ):
   """lat/lon -> address

   For Flask: Mapping layer between the web and geocode_reverse().

   Returns:
      result of calling geocode_reverse() with parameters extracted from flask.request.args
   """
   ans = ""

# required

   geoservice = _scrub ( geoservice)

   api_key = flask.request.args.get ( 'api_key' )
   lat     = float ( flask.request.args.get ( 'lat' ) )
   lon     = float ( flask.request.args.get ( 'lon' ) )

# optional

   limit = int ( flask.request.args.get ( 'limit', 3 ) )
   debug = flask.request.args.get ( 'debug' ) is not None

   if not geoservice:
      ans = ( "Bad Request -> missing required path parameter: 'geoservice'", 400 )

   elif api_key is None:
      ans = ( "Bad Request -> missing required query parameter: 'api_key'", 400 )

   elif lat is None:
      ans = ( "Bad Request -> missing required query parameter: 'lat'", 400 )
   elif lat < -90:
      ans = ( f"Bad Request -> value ({lat}) of query parameter 'lat' is below its minimum (-90)", 400 )
   elif lat > 90:
      ans = ( f"Bad Request -> value ({lat}) of query parameter 'lat' is above its maximum (90)", 400 )

   elif lon is None:
      ans = ( "Bad Request -> missing required query parameter: 'lon'", 400 )
   elif lon < -180:
      ans = ( f"Bad Request -> value ({lon}) of query parameter 'lon' is below its minimum (-180)", 400 )
   elif lon > 180:
      ans = ( f"Bad Request -> value ({lon}) of query parameter 'lon' is above its maximum (180)", 400 )

   elif limit < 1:
      ans = ( f"Bad Request -> value ({limit}) of query parameter 'limit' is below its minimum (1)", 400 )

   else:
      ans = geocode_reverse ( geoservice, api_key, lat, lon, limit, debug )

   if debug or isinstance ( ans, tuple ):
      print()
      print ( ">>>>>>>>>>>>>>>>>>>>>>>>>" )
      pp    ( flask.request.args.keys() )
      print ( "-------------------------" )
      print ( ans )
      print ( "<<<<<<<<<<<<<<<<<<<<<<<<<" )
      print()

   return ans


def geocode_reverse ( geoservice, api_key, lat, lon, limit, debug=False ):
   """lat/lon -> address

   Args:
     geoservice (str) : underlying geoservice provider
     api_key (str)    : caller's geoservice API key
     lat (float)      : latitude, such as 47.604849
     lon (float)      : longitude, such as -122.329722
     limit (int)      : number of matches to find
     debug (bool)     : print out debug info?

   Returns:
     dict: { 'status': { 'code':http-status-code, 'reason':http-reason }, 'invoke': { 'geoservice':geoservice, 'direction':"reverse", 'lat':latitude, 'lon':longitude }, 'matches': [ { 'lat':latitude, 'lon':longitude, 'weight':weight, 'callout':callout }, ... ] }

   The essential "reverse geo-locate" information returned is what the Wander API module has chosen to be of primary importance. It consists of a list of 'dict' instances built from four fields in the individual entries in the query response, one instance per entry:

     - latitude   : of the located resource
     - longitude  : of the located resource
     - weight     : rating between 0 and 1 (inclusive) where 1 is highest
     - callout    : the common name for this place

   This list of 'dict' instances is, in turn, assembled into an outer 'dict' instance that also includes the HTTP status information of the underlying MapBox request, plus the core information from the calling sequence.
   """

   _remaining      = 1
   _status_code_ok = True
   gather          = [ () ]

   if geoservice == "mapbox":
      params = {
         'access_token': api_key,
         'limit'       : limit,
         'types'       : 'address',
      }
      endpoint = f"https://api.mapbox.com/geocoding/v5/mapbox.places/{lon},{lat}.json"
   elif geoservice == "positionstack":
      params = {
         'access_key': api_key,
         'query'     : f"{lat},{lon}",
         'limit'     : limit,
      }
      endpoint = "http://api.positionstack.com/v1/reverse"
   else:
      params = {}
      endpoint = ""

   while _remaining >= 0 and not gather[0] and _status_code_ok:

      r = requests.get ( endpoint, params=params )

      if debug:
         print()
         print ( f">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> // {_remaining}" )
         print ( f"status_code={r.status_code} reason={r.reason}" )
         print ( "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" )
         pp ( params )

      if r.status_code == 200:
         if geoservice == "mapbox":
            info   = r.json()['features']
            gather = [ { 'lat':_['center'][1], 'lon':_['center'][0], 'weight':_['relevance'], 'callout':_['place_name'] } if _  else {} for _ in info ]
         elif geoservice == "positionstack":
            info   = r.json()['data']
            gather = [ { 'lat':_['latitude'], 'lon':_['longitude'], 'weight':_['confidence'], 'callout':_['label'] } if _  else {} for _ in info ]
         else:
            info   = ""
            gather = []

         if debug:
            print ( "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" )
            pp ( info )
         else:
            pass

         _remaining -= 1

      else:
         _status_code_ok = False

      if debug:
         print ( "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" )
         pp ( gather )
         print ( "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" )
         print()

   ans = {
      'status': { 'code':r.status_code, 'reason':r.reason },
      'invoke': { 'geoservice':geoservice, 'direction':"reverse", 'lat':lat, 'lon':lon },
      'matches': gather
   }

   return ans


#####################################################################################################
#####################################################################################################
#####################################################################################################


if __name__ == '__main__':

   app.run ( debug=True, port=5000 )
